# Don't use, deprecated, old.

# Small part of my ~/ Arch folder

My small part of Arch Linux configuration files and ~/ folder, e.g. for i3-gaps, polybar, termite.


# Download

Just use `git clone https://gitlab.com/thenetman/my-arch-config.git` to download all the configs, and copy them to your ~/.config folder.


# Dependencies

To use all of these configs/files, you'll have to get:

- i3-gaps
- polybar
- termite
- xcompmgr
- synaptics
- libinput
- rofi
- maim
- xclip
- nitrogen

[still searching for other...]

To use my polybar config with some icons, you'll have to get Font Awesome font.
You can download it by `sudo pacman -S ttf-font-awesome` on Arch Linux and other arch-based distro's like Manjaro.


# Screenshots

<img src="https://gitlab.com/thenetman/my-arch-config/-/raw/master/Screenshots%20of%20Desktop/1.png">
<img src="https://gitlab.com/thenetman/my-arch-config/-/raw/master/Screenshots%20of%20Desktop/2.png">
<img src="https://gitlab.com/thenetman/my-arch-config/-/raw/master/Screenshots%20of%20Desktop/3.png">


# The End.

You passed it! That's all the info about my configs.
